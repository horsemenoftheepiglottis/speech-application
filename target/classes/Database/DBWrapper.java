package Database;
//imports
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.io.*;
import java.lang.*;

//connection
public class DBWrapper{
    
    public static void main(String[] args)
    {
	try {
	    //Connect to database
	    
	    Connection connection = getConnection();
	   
	    //Input from user if new or old account
	    InputStreamReader isr = new InputStreamReader(System.in);
	    BufferedReader br = new BufferedReader(isr);
	    System.out.println("New or Old account?\n");
	    String account = br.readLine();
	    
	    //If user says they are new
	    if(account.equals("New"))
		{
		    //ask type of account they are making
		    System.out.println("Researcher, Therapist, CareGiver, or Developer?\n");
		    String accountType = br.readLine();
		    System.out.println("Username: ");
                    String username = br.readLine();
                    System.out.println("Password: ");
                    String password = br.readLine();

		    CreateAccount(connection, accountType, username, password);

		    if(accountType.equals("CareGiver"))
			{
			    System.out.println("Birthday (MM/DD/YYYY): ");
			    String birthday = br.readLine();

			    System.out.println("Sex (M/F): ");
			    String sex = br.readLine();

			    System.out.println("Race: ");
			    String race = br.readLine();

			    System.out.println("Nationality: ");
			    String nationality = br.readLine();

			    System.out.println("Speech Ability: ");
			    String speechAbility = br.readLine();

			    System.out.println("Diagnosis: ");
			    String diagnosis = br.readLine();

			    System.out.println("Therapist First Name: ");
			    String therapistFName = br.readLine();

			    System.out.println("Therapist Last Name: ");
			    String therapistLName = br.readLine();

			    CreateCGAccount(connection, birthday, sex, race, nationality, speechAbility, diagnosis, therapistFName, therapistLName);
			}
		}
	    //if user says they are old, acocunt will be searched for
	    else
		{
		    System.out.println("Username: ");
		    String username = br.readLine();
		    System.out.println("Password: ");
		    String password = br.readLine();
		    
		    String found = FindAccount(connection, username, password);
		    
		    //if found send to function with flag to search database
		    if(found.equals("Devloper") || found.equals("CareGiver") || found.equals("Therapist") || found.equals("Researcher"))
			{
			    System.out.println("Found");
			}
		}
	    
	    //when finished, close connection
	    closeConnection(connection);
	}
	catch(IOException | SQLException e) {
	    e.printStackTrace();
	}
    }
    
    //Connection to database
    //Possibly put information into an XML file that the program would read to connect.
    //XML file could be updated to change location, rather than the java code
    public static Connection getConnection()
    {
	//chrome-orb-222916:us-west1:speech-database
	Connection connection = null;
	try{
	    String instanceConnectionName = "chrome-orb-222916:us-west1:speech-database";
	    String databaseName = "ChildSpeech";
	    String username = "SpeechPathology";
	    String password = "SpeechPathology";

	    Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(
						     "jdbc:mysql://35.247.82.214/testDB_2?useSSL=false",
						     "SpeechPathology", "SpeechPathology");

	    //output info if connection was successful
	    System.out.println("Connected to: " + connection.getMetaData().getURL());
	    System.out.println("DB name: " + connection.getMetaData().getDatabaseProductName());
	    System.out.println("DB version: " + connection.getMetaData().getDatabaseProductVersion());
	    System.out.println("Driver: " + connection.getMetaData().getDriverName());
	    
	    //TEST: show tables once connected, will take out later
	    Statement statement = connection.createStatement();
	    ResultSet resultSet = statement.executeQuery("SHOW TABLES");
	    while (resultSet.next()) {
		System.out.println(resultSet.getString(1));
	    }

	}
	catch(SQLException | ClassNotFoundException e){
	    e.printStackTrace();
	}
	return connection;
    }
    
    public static void CreateAccount(Connection con, String accountType, String username, String password) throws IOException, SQLException
    {
	String query = null;
	Statement statement = con.createStatement();
        int rows;
	ResultSet resultSet;
	String prev_id;
	try{
	    InputStreamReader isr = new InputStreamReader(System.in);
	    BufferedReader br = new BufferedReader(isr);
	    System.out.println("First Name: ");
	    String fname = br.readLine();
	    System.out.println("Last Name: ");
	    String lname = br.readLine();
	    //Insert Password Hashing here
	    //Or create password hash function


	    
	    switch(accountType)
		{
		case "Researcher":
		    resultSet = statement.executeQuery("SELECT Researcher_ID FROM Researcher");
                    prev_id = "r0";
                    while (resultSet.next()) {
                        prev_id = resultSet.getString(1);
                    }
                    int Rid = Integer.parseInt(prev_id.replaceAll("[\\D]", "")) + 1;
		    query = "INSERT INTO Researcher (Researcher_ID, First_Name, Last_Name, Username, Password) VALUES ('" +
			Rid + "', '" + fname + "', '" + lname + "', '"+ username + "', '" + password + "');";

		    //query table
		    rows = statement.executeUpdate(query);
		    break;
    
		case "Developer":
		    resultSet = statement.executeQuery("SELECT Developer_ID FROM Developer");
		    prev_id = "d0";
		    while (resultSet.next()) {
			prev_id = resultSet.getString(1);
		    }
		    int Did = Integer.parseInt(prev_id.replaceAll("[\\D]", "")) + 1;
		    query = "INSERT INTO Developer (Developer_ID, First_Name, Last_Name, Username, Password) VALUES ('d" +
			+ Did + "', '" + fname + "', '" + lname + "', '"+ username + "', '" + password + "');";
		    //query table
		    
		    rows = statement.executeUpdate(query);                      
		    break;

		case "CareGiver":
		    resultSet = statement.executeQuery("SELECT CareGiver_ID FROM CareGiver");
                    prev_id = "c0";
                    while (resultSet.next()) {
                        prev_id = resultSet.getString(1);
                    }
                    int Cid = Integer.parseInt(prev_id.replaceAll("[\\D]", "")) + 1;
		    query = "INSERT INTO CareGiver (CareGiver_ID, First_Name, Last_Name, Username, Password) VALUES ('" + 
			Cid + "', '"  + fname + "', '" + lname + "', '"+ username + "', '" + password + "');";
		    //query table
		   
		    rows = statement.executeUpdate(query);
		    break;

		case "Therapist":
		    resultSet = statement.executeQuery("SELECT Therapist_ID FROM Therapist");
                    prev_id = "t0";
                    while (resultSet.next()) {
                        prev_id = resultSet.getString(1);
                    }
                    int Tid = Integer.parseInt(prev_id.replaceAll("[\\D]", "")) + 1;
		    query = "INSERT INTO Therapist (Therapist_ID, First_Name, Last_Name, Username, Password) VALUES ('" +
			Tid + "', '" +fname + "', '" + lname + "', '"+ username + "', '" + password + "');";
		      
		    //query table
		    rows = statement.executeUpdate(query);
		    break;                                     
		}
	    System.out.println(query);
	}
	catch(IOException | SQLException e) {
	    e.printStackTrace();
	}
    }

    public static void CreateCGAccount(Connection con, String birthday, String sex, String race, String nationality, String speechAbility, 
				       String diagnosis, String therapistFName, String therapistLName) throws SQLException
    {
	Statement statement = con.createStatement();
        ResultSet resultSet;

	try{
	    String query = "INSERT INTO Subject (Birthday, Sex, Race, Nationality, Speech_Ability, Diagnosis, Therapist_ID, CareGiver_ID) VALUES ('" +
		birthday + "', '" + sex + "', '" + race + "', '" + nationality + "', '" + speechAbility + "', '" + diagnosis + "', '" +
		"(SELECT Therapist_ID FROM Therapist WHERE First_Name = " + therapistFName + ",  Last_Name = " + therapistLName + "), LAST_INSERT_ID());";
	    //query table
	    resultSet = statement.executeQuery(query);
	}
	catch(SQLException e) {
	    e.printStackTrace();
	}
    }
    public static String FindAccount(Connection con, String username, String password) throws SQLException 
    {
	//could ask for type of account and that would be quicker with less code.	
	Statement stmt = null;
	String query1 = "SELECT username FROM CareGiver;";
	String query2 = "SELECT username FROM Researcher;";
	String query3 = "SELECT username FROM Therapist;";
	String query4 = "SELECT username FROM Developer;";
	String accType = "test";
	int foundFlag = 0;
	String check;
	ResultSet rs;

	//insert hash fuction for password here
	




	try {
	    stmt = con.createStatement();
	    rs = stmt.executeQuery(query1);
	    while (rs.next()) {
		check =rs.getString("username");
		if((check.compareTo(username)) == 0)
		    {
			//check password here			
			foundFlag = 1;
			return "CareGiver";
		    }
		
	    }
	    if(foundFlag == 0)
		{
		    stmt = con.createStatement();
		    rs = stmt.executeQuery(query2);
		    while (rs.next()) {
			check =rs.getString("username");
			if((check.compareTo(username)) == 0)
			    {
				//check password here
				foundFlag = 1;
				return "Researcher";
			    }

		    }
		    if(foundFlag == 0)
			{
			    stmt = con.createStatement();
			    rs = stmt.executeQuery(query3);
			    while (rs.next()) {
				check =rs.getString("username");
				if((check.compareTo(username)) == 0)
				    {
					//check password here 
					foundFlag = 1;
					return "Therapist";
				    }

			    }
			    if(foundFlag == 0)
				{
				    stmt = con.createStatement();
				    rs = stmt.executeQuery(query4);
				    while (rs.next()) {
					check =rs.getString("username");
					if((check.compareTo(username)) == 0)
					    {
						//check password here 
						foundFlag = 1;
						return "Developer";
					    }
				    }
				}
			}
		}

	} catch (SQLException e ) {
      	    e.printStackTrace();
	} finally {
	    if (stmt != null) { stmt.close(); }
	}

	return "none";
    }
    
    //Queries for Developers
    public static ResultSet Developers(Connection con, String DBName) throws SQLException
    {
	//insert queries that don't output personal information and personal information.

	Statement stmt = null;
       
	String query = "";
	stmt = con.createStatement();
	ResultSet rs = stmt.executeQuery(query);

	return rs;
    }

    //Quiries for Researchers
    public static ResultSet Researchers(Connection con, String DBName) throws SQLException
    {
	//insert queries to access audio files and personal information.
	Statement stmt = null;

        String query = "";
        stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query);

	return rs;
    }

    //Quiries for CareGivers
    public static ResultSet CareGivers(Connection con, String DBName) throws SQLException
    {
	//insert queries to access personal table and save audio files created.
	Statement stmt = null;
        
        String query = "";
        stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query);

	return rs;
    }
    
    //Quiries for Therapists
    public static ResultSet Therapists(Connection con, String DBName) throws SQLException
    {
	//insert queries to access personal table and tables connected info of their students.
	Statement stmt = null;
        
        String query = "";
        stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query);

	return rs;
    }

    private static void closeConnection(Connection connection) 
    {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                // ignore
                ;
            }
        }
    }

    private static void CSVCreate(Connection connection)
    {
	//for porting purposes
	//run from command line
	//create a CSV file from all data
	//input data into new database instance
    }
}