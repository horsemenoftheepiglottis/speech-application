package Database;

import Wrap.DBWrapper;

import java.sql.SQLException;
import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.io.FileReader;
import java.util.List;
import com.opencsv.*;

class main extends DBWrapper{

    public static void main(String[] args)
    {
	//this is all used for testing! Not needed for wrapper to function
	try {
            //Connect to database
	    DBWrapper db = new DBWrapper();
            Connection connection = db.getConnection("35.247.82.214", "SpeechPathology", "SpeechPathology", "testDB_2");

            //Input from user if new or old account
            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);
            System.out.println("New or Old account?\n");
            String account = br.readLine();

            //If user says they are new
            if(account.equals("New"))
                {
                    //ask type of account they are making
                    System.out.println("Researcher, Therapist, CareGiver, or Developer?\n");
                    String accountType = br.readLine();

		    setAccountType(accountType);

                    System.out.println("Username: ");
                    String username = br.readLine();
                    System.out.println("Password: ");
                    String password = br.readLine();

                    String computed_hash = db.hashPassword(password);
                    db.CreateAccount(connection, username, computed_hash, "SpeechPathology", "testDB_2");

		    //if they are a new Caregiver they need to fill out a section in the subject table
                    if(accountType.equals("CareGiver"))
			{
                            System.out.println("Birthday (MM/DD/YYYY): ");
                            String birthday = br.readLine();

                            System.out.println("Sex (M/F): ");
                            String sex = br.readLine();

                            System.out.println("Race: ");
                            String race = br.readLine();

                            System.out.println("Nationality: ");
                            String nationality = br.readLine();

                            System.out.println("Speech Ability: ");
                            String speechAbility = br.readLine();

                            System.out.println("Diagnosis: ");
                            String diagnosis = br.readLine();

                            System.out.println("Therapist First Name: ");
                            String therapistFName = br.readLine();

                            System.out.println("Therapist Last Name: ");
                            String therapistLName = br.readLine();

			    int id = db.getID();
                            db.AddSubjectInfo(connection, birthday, sex, race, nationality, speechAbility, diagnosis, therapistFName, therapistLName, id);
                        }
		}

	    //if user says they are old, account will be searched for
            else
                {
                    int flag = 0;
                    while (flag == 0)
                        {
                            System.out.println("Username: ");
                            String username = br.readLine();
                            System.out.println("Password: ");
                            String password = br.readLine();

                            //String computed_hash = db.hashPassword(password);
                            String found = db.FindAccount(connection, username, password, "SpeechPathology", "testDB_2");

                            //if found send to function with flag to search database
                            if(found.equals("Developer") || found.equals("CareGiver") || found.equals("Therapist") || found.equals("Researcher"))
                                {
                                    System.out.println("Found");
                                    AccountType = found;
                                    flag = 1;
                                }
                            else
                                {
                                    System.out.println("Account not identified. Retry or exit?");
                                    String answer = br.readLine();
                                    if(answer.equals("exit"))
                                        {
                                            System.exit(0);
                                        }
                                }
                        }
                }

            System.out.println("Update Account [y or n]: ");
            String response = br.readLine();
	    //what to update account?
            if(response.equals("y"))
                {
                    System.out.println("_Password or Username? ");
                    response = br.readLine();
                    System.out.println("What would you like to change to? ");
                    String updatedinfo = br.readLine();

                    System.out.println("First Name: ");
                    String fname = br.readLine();
                    System.out.println("Last Name: ");
                    String lname = br.readLine();

		    //if they want to change their password
                    if(response.equals("password"))
                        {
                            System.out.println("what is your username? ");
                            String un = br.readLine();
			    db.UpdateDeveloper(connection, response, updatedinfo, un, fname, lname, "SpeechPathology", "testDB_2");
                        }
		    //if they want to change their username
                    else if(response.equals("username"))
                        {
                            System.out.println("what is your password? ");
                            String pw = br.readLine();

                            db.UpdateDeveloper(connection, response, updatedinfo, pw, fname, lname, "SpeechPathology", "testDB_2");
                        }
                }
            System.out.println("complete");

	    //Want to port out?
	    System.out.println("Port? [y or n]: ");
            response = br.readLine();
            if(response.equals("y"))
                {
		    Connection new_con;
		    db.CSVCreate(connection);
		    new_con = db.getConnection("35.247.82.214", "SpeechPathology", "SpeechPathology", "ChildSpeech");
		    if(new_con != null){
			db.DatabaseCreate(new_con, "ChildSpeech");
			db.CSVImport(new_con);
			db.closeConnection(new_con);
		    }
		}

	    System.out.println("Rebuild? [y or n]: ");
            response = br.readLine();
            if(response.equals("y")){
                db.DatabaseCreate(connection, "testDB_2");
            }

	    System.out.println("Import files? [y or n]: ");
	    response = br.readLine();
	    if(response.equals("y")){
		ImportDataFiles(connection);
	    }

            //when finished, close connection
            db.closeConnection(connection);

            System.exit(0);
        }
        catch(IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static void ImportDataFiles(Connection con) throws IOException, SQLException
    {
	DBWrapper db = new DBWrapper();

	String sex = "";
	String ext = "";
	String filename = "";
	String filesize = "";
	String duration = "";
	String samplerate = "";
	String bitdepth = "";
	String ASRTrans = "";
	String ASRPrompt = "";
	String ASRhyp = "";
	String audioformat = "*.wav";
	String channels = "";
	String noise_pro = "";
	String purpose = "";
	String date = "";
	String resercher_id = "";
	String sess_id = "";

	String tname = "";
	String hname = "";

	String fext = "";
	String ffilename = "";
	String ffilesize = "";
	String fduration = "";
	String fsamplerate = "";
	String fbitdepth = "";
	String fASRTrans = "";
	String fASRhyp = "";
	String fchannels = "";
	int count = 0;

	String name = "null";

	int dev_id = db.getID();
	String sid = "0";

	//files are name.csv, point.csv, trans.csv, header.csv
	try{
	    ///Users/haileyjohnson/SpeechPathology/speech-application/src/main/java/Database/
	    FileReader fr1 = new FileReader("/home/johnshai001/speech-application/src/main/java/Database/name.csv");
	    CSVReader csv1 = new CSVReader(fr1);
            String[] nextRec1;

	    FileReader fr2 = new FileReader("/home/johnshai001/speech-application/src/main/java/Database/header.csv");
	    CSVReader csv2 = new CSVReader(fr2);
            String[] nextRec2;

	    FileReader fr3 = new FileReader("/home/johnshai001/speech-application/src/main/java/Database/trans.csv");
	    CSVReader csv3 = new CSVReader(fr3);
            String[] nextRec3;

	    FileReader fr4 = new FileReader("/home/johnshai001/speech-application/src/main/java/Database/point.csv");
	    CSVReader csv4 = new CSVReader(fr4);
            String[] nextRec4;

	    int flag = 0;
	    int innerflag = 0;
	    int innerflag2 = 0;
	    int innerflag3 = 0;
	    int innerflag4 = 0;
	    String query = "null";

      //skips start line
	    nextRec1 = csv1.readNext();
	    nextRec2 = csv2.readNext();
	    nextRec3 = csv3.readNext();
	    nextRec4 = csv4.readNext();

	    while(flag == 0)
		{
		    if((nextRec1 = csv1.readNext()) != null) {
			for (String cell : nextRec1) {
			    if(cell.length() == 4)
				{
				    name = cell;
				    System.out.println(name);
				}
			    else
				{
				    sex = cell;
				    System.out.println(sex);
				}
			}
		    }
		    else {
			flag = 1;
			System.out.println("flag changed");
		    }

		    if(flag != 1)
			{
			    query = "INSERT INTO SubjectInfo (Birthday, Sex, Race, Nationality, Speech_Ability, Diagnosis, Therapist_ID, CareGiver_ID) VALUES (" +
				"NULL, '" + sex  + "', NULL, NULL, NULL, NULL, NULL, NULL);";
			    System.out.println(query);
			    int check = db.UpdateQuery(con, query);
			   
			    query = "SELECT max(Subject_ID) FROM SubjectInfo;";
			    ResultSet rs = db.SelectQuery(con, query);
			    while(rs.next())
				{
				    sid = rs.getString(1);
				    System.out.println(sid);
				}
			    if(check == -1)
				{
				    System.out.println("Update Failed");
				}
			    else if(check == 0)
				{
				    System.out.println("No rows effected");
				}
			    else
				{
				    System.out.println("Update Success. Number of rows changed" + check);
				}
			}

		    if(count > 0 && flag != 1)
			{
			    System.out.println("outerloop ffilename: " + ffilename + " " + ffilename.substring(0,4) + " " + name);
			    if(name.equals(ffilename.substring(0,4)))
				{
				    query = "INSERT INTO Audio (AudioDate, Raw_Audio_Root, Raw_Audio_FileName, Audio_Format, Purpose, Noise_Profile, Channels, Sample_Rate, Duration_time, File_Size, Bit_Depth, ASR_Version, ASR_Trascript, ASR_Prompt, ASR_Hypothesis, Researcher_ID, Developer_ID, Session_ID, Subject_ID) VALUES ('" +
            "', '" + fext + "', '" + ffilename + "', '" + audioformat + "', '" + purpose + "', '" + noise_pro + "', '" + fchannels + "', '" + fsamplerate + "', '" + fduration + "', '" + ffilesize + "', '" + fbitdepth + "', NULL, '" + fASRTrans + "', '" + ASRPrompt + "', '" + fASRhyp + "', NULL, '" + dev_id + "', NULL, '" + sid + "');";
				    System.out.println(query);
				    int check = db.UpdateQuery(con, query);
                                    if(check == -1)
                                        {
                                            System.out.println("Update Failed");
                                        }
                                    else if(check == 0)
                                        {
                                            System.out.println("No rows effected");
                                        }
                                    else
                                        {
                                            System.out.println("Update Success. Number of rows changed = " + check);
                                        }

				    fext = "";
				    ffilename = "";
				    ffilesize = "";
				    fduration = "";
				    fsamplerate = "";
				    fbitdepth = "";
				    fASRTrans = "";
				    fASRhyp = "";
				    fchannels = "";
				}
			}
		    String hhold = "null";
		    String thold = "null";
		    while(innerflag == 0 && flag != 1)
			{
                            ext = "";
                            filename = "";
                            filesize = "";
                            duration = "";
                            samplerate = "";
                            bitdepth = "";
                            ASRTrans = "";
                            ASRPrompt = "";
                            ASRhyp = "";
                            channels = "";


			    if(innerflag2 == 0){
				if(((nextRec2 = csv2.readNext()) != null) && (nextRec2[0].equals(name)))
				    {
					filename = nextRec2[1];
					ext = nextRec2[2];
					channels = nextRec2[4];
					samplerate = nextRec2[5];
					bitdepth = nextRec2[6].substring(0,2);
					duration = nextRec2[7].substring(0,11);
					filesize = nextRec2[8];
					filesize = filesize.substring(0, filesize.length() -1);

					System.out.println("parsed header: " + ext + " " + filename + " " + channels + " " + samplerate + " " + bitdepth + " " + duration + " " + filesize);
				    }
				else
				    {
					if((nextRec2 != null) && !(nextRec2[0].equals(name)))
					    {
						ffilename = nextRec2[1];
						fext = nextRec2[2];
						fchannels = nextRec2[4];
						fsamplerate = nextRec2[5];
						fbitdepth = nextRec2[6].substring(0,2);
						fduration = nextRec2[7].substring(0,11);
						ffilesize = nextRec2[8];
						ffilesize = ffilesize.substring(0, ffilesize.length() -1);

						System.out.println("update header: name = " + nextRec2[0] + " " + fext + " " + ffilename + " " + fchannels + " " + fsamplerate + " " + fbitdepth + " " + fduration + " " + ffilesize);

					    }
					innerflag2 = 1;
				    }
			    }


			    if(innerflag3 == 0)
				{
				    if(thold != "null")
					{
					    System.out.println("thold activated " + thold);
					    if(thold.compareTo(filename) == 0)
						{
						    System.out.println("filename is equal to thold"  + thold + " " + filename + "\n");
						    ASRTrans = thold;
						    thold = "null";
						}
					    else if(thold.compareTo(filename) < 0)
						{
						    int go = 1;
                                                    while(go == 1)
                                                        {
                                                            if(((nextRec3 = csv3.readNext()) != null) && (nextRec3[0].compareTo(name) == 0))
                                                                {
								    tname = nextRec3[1] + ".wav";
                                                                    if(tname.compareTo(filename) == 0)
                                                                        {
									    System.out.println("filename is equal to thold in while "  + thold + " " + filename + "\n");
                                                                            ASRTrans = nextRec3[2];
									    thold = "null";
                                                                            go = 0;
                                                                        }
                                                                    else if(tname.compareTo(filename) > 0)
                                                                        {
									    System.out.println("filename is farther to thold in while "  + thold + " " + filename + "\n");
                                                                            thold = tname;
                                                                            go = 0;
                                                                        }
                                                                }
                                                            else if((nextRec3 != null) && (nextRec3[0].compareTo(name) > 0))
                                                                {
								    tname =nextRec3[1] + ".wav";
                                                                    innerflag3 = 1;
								    if(tname.compareTo(filename) == 0)
									fASRTrans = tname;
                                                                    go = 0;
                                                                }
                                                        }
						}
					}
				    //name is correct
				    else if(((nextRec3 = csv3.readNext()) != null) && (nextRec3[0].compareTo(name) == 0))
					{
					    tname = nextRec3[1] + ".wav";
					    System.out.println("name is correct"  + nextRec3[0] + " " + name + "\n");
					    if(tname.compareTo(filename) == 0)
						{
						    System.out.println("filename is correct"  + nextRec3[1] + " " + filename + "\n");
						    ASRTrans = nextRec3[2];
						}
					    else if(tname.compareTo(filename) > 0)
						{
						    System.out.println("filename is farther"  + nextRec3[1] + " " + filename + "\n");
						    thold = tname;
						}
					    else if(tname.compareTo(filename) < 0)
						{
						    int go = 1;
						    System.out.println("filename is previous"  + nextRec3[1] + " " + filename + "\n");
						    while(go == 1)
							{
							    if(((nextRec3 = csv3.readNext()) != null) && (nextRec3[0].compareTo(name) == 0))
								{
								    tname =nextRec3[1] + ".wav";
								    if(tname.compareTo(filename) == 0)
									{
									    System.out.println("filename is correct in while loop"  + nextRec3[1] + " " + filename + "\n");
									    ASRTrans = nextRec3[2];
									    go = 0;
									}
								    else if(tname.compareTo(filename) > 0)
									{
									    System.out.println("filename is farther in while"  + nextRec3[1] + " " + filename + "\n");
									    thold = tname;
									    go = 0;
									}
								}
							    else if((nextRec3 != null) && (nextRec3[0].compareTo(name) > 0))
								{
								    tname =nextRec3[1] + ".wav";
								    innerflag3 = 1;
								    if(tname.compareTo(filename) == 0)
									fASRTrans = tname;
								    go = 0;
								}
							}
						}
					}
				    //name later than current name
				    else if((nextRec3 != null) && (nextRec3[0].compareTo(name) > 0))
					{
					    tname = nextRec3[1] + ".wav";
					    System.out.println("name is later than current name " + nextRec3[0] + " " + name + "\n");
					    innerflag3 = 1;
					    if(tname.compareTo(filename) == 0)
						fASRTrans = tname;
					}
				    //name in csv is before current name
				    else if ((nextRec3 != null) && (nextRec3[0].compareTo(name) < 0))
					{
					    System.out.println("name is before than current name " + nextRec3[0] + " " + name + "\n");
					    int go = 1;
					    while(go == 1)
						{
                                                    if(((nextRec3 = csv3.readNext()) != null) && (nextRec3[0].compareTo(name) == 0))
							{
							    tname = nextRec3[1] + ".wav";
							    if(tname.compareTo(filename) == 0)
								{
								    System.out.println("filename is correct in while loop "  + nextRec3[1] + " " + filename + "\n");
								    ASRTrans = nextRec3[2];
								    go = 0;
								}
							    else if(tname.compareTo(filename) > 0)
								{
								    System.out.println("filename is farther in while "  + nextRec3[1] + " " + filename + "\n");
								    thold = tname;
								    go = 0;
								}
							}
						    else if((nextRec3 != null) && (nextRec3[0].compareTo(name) > 0))
							{
							    tname = nextRec3[1] + ".wav";
							    go = 0;
							    innerflag3 = 1;
							    if(tname.compareTo(filename) == 0)
								fASRTrans = tname;
							}
						}
					}
				}


			    if(innerflag4 == 0)
				{
				    if(hhold != "null")
					{
					    System.out.println("hhold activated " + hhold);
					    if(hhold.compareTo(filename) == 0)
						{
						    System.out.println("filename is equal to hhold"  + hhold + " " + filename + "\n");
						    ASRhyp = hhold;
						    hhold = "null";
						}
					    else if(hhold.compareTo(filename) < 0)
						{
						    int go = 1;
                                                    while(go == 1)
                                                        {
                                                            if(((nextRec4 = csv4.readNext()) != null) && (nextRec4[0].compareTo(name) == 0))
                                                                {
								    hname = nextRec4[1] + ".wav";
                                                                    if(hname.compareTo(filename) == 0)
                                                                        {
									    System.out.println("filename is equal to hhold in while "  + hhold + " " + filename + "\n");
                                                                            ASRhyp = nextRec4[2];
									    hhold = "null";
                                                                            go = 0;
                                                                        }
                                                                    else if(hname.compareTo(filename) > 0)
                                                                        {
									    System.out.println("filename is farther to hhold in while "  + hhold + " " + filename + "\n");
                                                                            hhold = hname;
                                                                            go = 0;
                                                                        }
                                                                }
                                                            else if((nextRec4 != null) && (nextRec4[0].compareTo(name) > 0))
                                                                {
								    hname =nextRec4[1] + ".wav";
                                                                    innerflag4 = 1;
								    if(hname.compareTo(filename) == 0)
									fASRhyp = hname;
                                                                    go = 0;
                                                                }
                                                        }
						}
					}
				    //name is correct
				    else if(((nextRec4 = csv4.readNext()) != null) && (nextRec4[0].compareTo(name) == 0))
					{
					    hname = nextRec4[1] + ".wav";
					    System.out.println("name is correct"  + nextRec4[0] + " " + name + "\n");
					    if(hname.compareTo(filename) == 0)
						{
						    System.out.println("filename is correct"  + nextRec4[1] + " " + filename + "\n");
						    ASRhyp = nextRec4[2];
						}
					    else if(hname.compareTo(filename) > 0)
						{
						    System.out.println("filename is farther"  + nextRec4[1] + " " + filename + "\n");
						    hhold = hname;
						}
					    else if(hname.compareTo(filename) < 0)
						{
						    int go = 1;
						    System.out.println("filename is previous"  + nextRec4[1] + " " + filename + "\n");
						    while(go == 1)
							{
							    if(((nextRec4 = csv4.readNext()) != null) && (nextRec4[0].compareTo(name) == 0))
								{
								    hname =nextRec4[1] + ".wav";
								    if(hname.compareTo(filename) == 0)
									{
									    System.out.println("filename is correct in while loop"  + nextRec4[1] + " " + filename + "\n");
									    ASRhyp = nextRec4[2];
									    go = 0;
									}
								    else if(hname.compareTo(filename) > 0)
									{
									    System.out.println("filename is farther in while"  + nextRec4[1] + " " + filename + "\n");
									    hhold = hname;
									    go = 0;
									}
								}
							    else if((nextRec4 != null) && (nextRec4[0].compareTo(name) > 0))
								{
								    hname =nextRec4[1] + ".wav";
								    innerflag4 = 1;
								    if(hname.compareTo(filename) == 0)
									fASRhyp = hname;
								    go = 0;
								}
							}
						}
					}
				    //name later than current name
				    else if((nextRec4 != null) && (nextRec4[0].compareTo(name) > 0))
					{
					    hname =nextRec4[1] + ".wav";
					    System.out.println("name is later than current name " + nextRec4[0] + " " + name + "\n");
					    innerflag4 = 1;
					    if(hname.compareTo(filename) == 0)
						fASRhyp = hname;
					}
				    //name in csv is before current name
				    else if ((nextRec4 != null) && (nextRec4[0].compareTo(name) < 0))
					{
					    System.out.println("name is before than current name " + nextRec4[0] + " " + name + "\n");
					    int go = 1;
					    while(go == 1)
						{
                                                    if(((nextRec4 = csv4.readNext()) != null) && (nextRec4[0].compareTo(name) == 0))
							{
							    hname = nextRec4[1] + ".wav";
							    if(hname.compareTo(filename) == 0)
								{
								    System.out.println("filename is correct in while loop "  + nextRec4[1] + " " + filename + "\n");
								    ASRhyp = nextRec4[2];
								    go = 0;
								}
							    else if(hname.compareTo(filename) > 0)
								{
								    System.out.println("filename is farther in while "  + nextRec4[1] + " " + filename + "\n");
								    hhold = hname;
								    go = 0;
								}
							}
						    else if((nextRec4 != null) && (nextRec4[0].compareTo(name) > 0))
							{
							    hname = nextRec4[1] + ".wav";
							    go = 0;
							    innerflag4 = 1;
							    if(hname.compareTo(filename) == 0)
								fASRhyp = hname;
							}
						}
					}
				}



			    if(innerflag2 == 1)
				{
				    System.out.println("UPDATED innerflag");
				    innerflag = 1;
				}

			    if(innerflag == 0)
				{
				    query = "INSERT INTO Audio (AudioDate, Raw_Audio_Root, Raw_Audio_FileName, Audio_Format, Purpose, Noise_Profile, Channels, Sample_Rate, Duration_time, File_Size, Bit_Depth, ASR_Version, ASR_Trascript, ASR_Prompt, ASR_Hypothesis, Researcher_ID, Developer_ID, Session_ID, Subject_ID) VALUES ('" +
					"', '" + ext + "', '" + filename + "', '" + audioformat + "', '" + purpose + "', '" + noise_pro + "', '" + channels + "', '" + samplerate + "', '" + duration + "', '" + filesize + "', '" + bitdepth + "', NULL, '" + ASRTrans + "', '" + ASRPrompt + "', '" + ASRhyp + "', NULL, '" + dev_id + "', NULL, '" + sid + "');";

				    System.out.println(query);

				    int check = db.UpdateQuery(con, query);
				    if(check == -1)
					{

					    System.out.println("Update Failed");
					}
				    else if(check == 0)
					{
					    System.out.println("No rows effected");
					}
				    else
					{
					    System.out.println("Update Success. Number of rows changed = " + check);
					}
				}

			    System.out.println("end of inner loop ffilename: " + ffilename);
			}
		    innerflag = 0;
		    innerflag2 = innerflag3 = innerflag4 = 0;
		    count++;
		    sex = "";
		    name = "";
		}
	}
	catch(IOException e ) { //| SQLException e){
	    e.printStackTrace();
	}
    }
}
